scikit-image==0.21.0
planetary-computer==0.4.9
pystac-client==0.6.0
pystac==1.7.0
numpy==1.22.3
xarray==2022.9.0
rioxarray==0.13.4
pandas==2.0.3
seaborn==0.12.2
# scikit-learn==0.20.3
rasterio==1.3.8
pillow==9.2.0
shapely==1.8.4
fiona==1.8.21
geopandas==0.12.2
streamlit==1.32.2
streamlit-folium==0.22.0
folium==0.14.0
tifffile==2022.10.10
odc-stac==0.3.7
altair==4.0
pydantic<2
stqdm
# bfast==0.7
imageio==2.31.2
scipy==1.11.1
streamlit-image-coordinates==0.1.6
# GDAL==3.5.2
matplotlib==3.7.2
wget==3.2
Sphinx==2.2.0
