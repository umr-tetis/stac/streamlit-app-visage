FROM continuumio/miniconda3:4.12.0
RUN conda install python==3.9

COPY requirements.txt . 

# on ajoute tout ce qui existe à la racine dans app
ADD . /app/

# ENV PYTHONPATH=/opt/otb/lib/python3/dist-packages/:$PYTHONPATH
RUN python3 -m pip install -r requirements.txt

RUN python3 -m pip install pyopencl[pocl] scikit-learn

RUN python3 -m pip install bfast==0.7
 # python-pyopencl

# RUN pip install pyopencl
# RUN pip install bfast==0.7

WORKDIR /app 

HEALTHCHECK CMD curl --fail http://localhost:8501/_stcore/health


# # ouvre le port
EXPOSE 8501
# ENTRYPOINT ["/opt/conda/bin/python","streamlit", "run", "Home.py", "--server.port=8501", "--server.address=0.0.0.0"]

ENTRYPOINT ["streamlit", "run", "Home.py", "--server.port=8501", "--server.address=0.0.0.0"]



