import streamlit as st
import os
import re

def lister_fichiers_dossier_1(dossier_path):
    fichiers = []
    with os.scandir(dossier_path) as entries:
        for entry in entries:
            if entry.is_file() and entry.name.startswith("1_"):
                fichiers.append(entry.name)
    return fichiers


def lister_fichiers_dossier_2(dossier_path):
    fichiers = []
    with os.scandir(dossier_path) as entries:
        for entry in entries:
            if entry.is_file() and entry.name.startswith("2_"):
                fichiers.append(entry.name)
    return fichiers

def lister_dossiers(dossier_path):
    dossiers = []
    with os.scandir(dossier_path) as entries:
        for entry in entries:
            if entry.is_dir():
                dossiers.append(entry.name)
    return dossiers

def lister_fichiers_specifiques(dossier_path, extensions):
    fichiers_specifiques = []
    with os.scandir(dossier_path) as entries:
        for entry in entries:
            if entry.is_file():
                for extension in extensions:
                    if entry.name.endswith(extension):
                        fichiers_specifiques.append(entry.name)
                        break  # Sortir de la boucle une fois qu'une correspondance est trouvée

    return fichiers_specifiques

def lister_fichiers(dossier_path):
    fichiers = []
    with os.scandir(dossier_path) as entries:
        for entry in entries:
            if entry.is_file() :
                fichiers.append(entry.name)
    return fichiers


def lister_fichiers_RGB(dossier_path):
    fichiers_cp = []
    with os.scandir(dossier_path) as entries:
        for entry in entries:
            if entry.is_file() and (entry.name.endswith("stack.tif")):
                fichiers_cp.append(entry.name)
    return fichiers_cp


def lister_fichiers_date(dossier_path):
    fichiers_cp = []
    with os.scandir(dossier_path) as entries:
        for entry in entries:
            if entry.is_file() and (entry.name.endswith("date.tif")):
                fichiers_cp.append(entry.name)
    return fichiers_cp

def download_file(file_path, file_name):
    with open(file_path, 'rb') as f:
        data = f.read()
    st.download_button(label="Download " + file_name, data=data, file_name=file_name)



def main():
    st.title("Download")
    st.title('Raw NDVI time series')
    dossier_path = "pages/data"
    # Chemin vers le fichier nc
    fichiers_dans_dossier = lister_fichiers_dossier_1(dossier_path)
    fichier_selectionne = st.selectbox("Select a file:", fichiers_dans_dossier)
    file_path = f"pages/data/{fichier_selectionne}"
    file_name = fichier_selectionne
    if file_name:
    # Afficher le bouton de téléchargement
        download_file(file_path, file_name)

    
    st.title('Smoothed NDVI time series')
    dossier_path = "pages/data"
    # Chemin vers le fichier nc
    fichiers_dans_dossier = lister_fichiers_dossier_2(dossier_path)
    fichier_selectionne = st.selectbox("Select a file:", fichiers_dans_dossier , key=2)
    file_path = f"pages/data/{fichier_selectionne}"
    file_name = fichier_selectionne
    if file_name:
    # Afficher le bouton de téléchargement
        download_file(file_path, file_name)

    st.title('Stack image of all metrics (magnitude, ndvi ratio, dissimilarity, date)')
    dossier_path = "pages/data"
    # Chemin vers le fichier nc
    fichiers_dans_dossier = lister_fichiers_RGB(dossier_path)
    fichier_selectionne = st.selectbox("Select a file:", fichiers_dans_dossier , key=8)
    file_path = f"pages/data/{fichier_selectionne}"
    file_name = fichier_selectionne
    if file_name:
    # Afficher le bouton de téléchargement
        download_file(file_path, file_name)











if __name__ == "__main__":
    main()

