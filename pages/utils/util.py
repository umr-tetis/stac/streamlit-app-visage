import numpy as np
import xarray as xr
import pandas as pd
import os
import math
from scipy.spatial import procrustes
import logging
logger = logging.getLogger(__name__)


def date_before_and_after(date_np:np.datetime64, years:int):
    # Convertir le numpy datetime en objet pandas Timestamp
    date_pd = pd.to_datetime(date_np)

    # Calculer la date 3 ans avant
    date_before = date_pd - pd.DateOffset(years=years)

    # Calculer la date 3 ans après
    date_after = date_pd + pd.DateOffset(years=years)

    # Convertir les résultats en numpy datetime
    date_before_np = np.datetime64(date_before)
    date_after_np = np.datetime64(date_after)

    return date_before_np, date_after_np

def lister_fichiers_dossier(dossier_path):
    fichiers = []
    with os.scandir(dossier_path) as entries:
        for entry in entries:
            if entry.is_file() and entry.name.startswith("2_"):
                fichiers.append(entry.name)
    return fichiers

  
def calculate_l2_distance(time: xr.DataArray, ndvi_smooth: xr.DataArray, int_break_arr: list):
    # Description : Cette fonction calcule la distance L2 entre les données NDVI lissées et les points de rupture détectés.
    # Entrée : time - Tableau des dates, ndvi_smooth - Tableau de données NDVI lissées, int_break_arr - Liste des indices de points de rupture.
    # Sortie : liste_l2_dist - Liste des distances L2 calculées, breakpointmax - Indice du point de rupture maximal.

    date_str = pd.to_datetime(time[int_break_arr].values)
    date_before = date_str - pd.DateOffset(years=3)
    date_after = date_str + pd.DateOffset(years=3)
    
    date_before_str = np.datetime_as_string(date_before, unit='D')
    date_after_str = np.datetime_as_string(date_after, unit='D')

    ds_monthly_before = []
    ds_monthly_after = []
    
    for ind, index_to_highlight in enumerate(int_break_arr):
        #Annee mean sur les 3 ans avant et apres le breakpoint
        selected_data_before = ndvi_smooth.sel(time=slice(date_before_str[ind], time.values[index_to_highlight]))
        ds_monthly_before.append(selected_data_before.groupby('time.month').mean(dim='time').values)

        selected_data_after = ndvi_smooth.sel(time=slice(time.values[index_to_highlight], date_after_str[ind]))
        ds_monthly_after.append(selected_data_after.groupby('time.month').mean(dim='time').values)
        
    ds_monthly_before = np.stack(ds_monthly_before)
    ds_monthly_after = np.stack(ds_monthly_after)    

    l2_distance = np.linalg.norm(ds_monthly_before - ds_monthly_after,axis=1)
    breakpointmax = int_break_arr[np.argmax(l2_distance)]
        
    return l2_distance,  breakpointmax

def process_dataset(dataset: xr.DataArray):
    # Créez un masque booléen où les valeurs égales à zéro sont True
    mask = np.isnan(dataset.ndvi_smooth) | (dataset.ndvi_smooth == 0)
    
    # Appliquez le masque pour filtrer les données
    filtered_data = dataset.where(~mask, drop=True)

    # Extraire les coordonnées temporelles du dataset
    dates = filtered_data['time'].values
    logger.info(f"Dropping {dataset.time - len(dataset.time)} dates (ndvi == 0)")
    # Vous pouvez également convertir les valeurs de dates en objets datetime si nécessaire
    datetime_object = [date.astype('M8[ms]').astype('O') for date in dates]
    
    ndvi_values = filtered_data['ndvi_smooth'].values
    ndvi_values = ndvi_values.astype(int)
    
    return datetime_object, ndvi_values , filtered_data


def calculate_ndviratio_magni_pond_disparity(time, ndvi_smooth, index_to_highlight):
    #  Description : Cette fonction calcule le ratio NDVI pour les changements détectés.
    #  Entrée : time: Tableau des dates, ndvi_smooth: Tableau de données NDVI lissées, index_to_highlight: Indice du point de rupture sélectionné.
    
    # Sortie : abs_result - Résultat du calcul de la magnitude pondérée.
    date_str = time.values[index_to_highlight]
    date_before, date_after = date_before_and_after(date_str, 3)

    date_before_str = np.datetime_as_string(date_before, unit='D')
    date_after_str = np.datetime_as_string(date_after, unit='D')

    date_before_str = str(date_before_str)
    date_after_str = str(date_after_str)

    selected_data_before = ndvi_smooth.sel(time=slice(date_before_str, time.values[index_to_highlight]))
    ds_yearly_before = selected_data_before.groupby('time.year').mean(dim='time').values
    ds_monthly_before = selected_data_before.groupby('time.month').mean(dim='time')

    selected_data_after = ndvi_smooth.sel(time=slice(time.values[index_to_highlight], date_after_str))
    ds_yearly_after = selected_data_after.groupby('time.year').mean(dim='time').values
    ds_monthly_after = selected_data_after.groupby('time.month').mean(dim='time')

    # try:
    #     #meanNDVI_absLogRatio=abs(np.log(ds_yearly_after/ds_yearly_before))[0]
    #     meanNDVI_absLogRatio = selected_data_after.values.mean() / selected_data_before.values.mean()
    # except :
    #     meanNDVI_absLogRatio = np.nan
        
    # if meanNDVI_absLogRatio<0:
    #     meanNDVI_absLogRatio = np.nan
        
        
    # compute abs_result
    before_after_mean = selected_data_after.mean(dim='time') / selected_data_before.mean(dim='time')
    
    if before_after_mean < 0:
        before_after_mean= np.nan
        print(f"warning < 0 values for mean by year")
        
    result = math.log(before_after_mean)
    abs_result = abs(result)
    
    # compute disparity (or procruste)
    df_before = pd.DataFrame(ds_monthly_before)
    df_after = pd.DataFrame(ds_monthly_after)
    
    # make sur NaN values 
    # Combine both dataframes and drop rows with NaN in any of the dataframes
    combined_df = pd.concat([df_before, df_after], axis=1, keys=['before', 'after'])
    combined_df = combined_df.dropna()

    # Split the combined dataframe back into the original two dataframes
    df_before = combined_df['before'].reset_index(drop=True)
    df_after = combined_df['after'].reset_index(drop=True)
    
    try:
        mtx1, mtx2, procruste = procrustes(df_before, df_after)
    except:
        procruste=0 #todo bricolage
        
    
    return before_after_mean, abs_result, procruste


def process_pixel_changes(filtered_data: xr.Dataset, changes: np.ndarray, x: int, y: int):
    # Description : Cette fonction traite les changements détectés pour un pixel spécifique.
    # Entrée : filtered_data - Données raster filtrées, changes - Tableau des changements détectés, x - Coordonnée x du pixel, y - Coordonnée y du pixel.
    # Sortie : int_break_arr - Array des indices des points de rupture pour le pixel sélectionné, time - Tableau des dates, ndvi_smooth - Tableau de données NDVI lissées.
    # input : 
    # x,y  : coordinates 
    # changes : width x heigh x dates (nb de dates du monitoring de bfast)?
    
    x_coord = filtered_data.x.values[x]
    y_coord = filtered_data.y.values[y]
    changes_pixel = changes[y,x]
    
    # Select all temporal values for the specified pixel using .sel()
    subdataset = filtered_data.sel(x=x_coord, y=y_coord)
    
    # Access the smoothed NDVI variable for the selected pixel
    ndvi_smooth = subdataset['ndvi_smooth']
    
    # Access the temporal coordinates (time) of the smoothed NDVI variable
    time = ndvi_smooth['time']
    
    # Remove NaN values and convert to integers
    # int_break_arr = sorted([int(c) for c in changes_pixel if not math.isnan(c)])
    int_break_arr = np.sort(changes_pixel[~np.isnan(changes_pixel)]).astype("int")
    
    if len(int_break_arr) == 0:
        return int_break_arr, time, ndvi_smooth

    last_date = time[-1].values
    break_date_df = pd.to_datetime(time[int_break_arr].values)

    # Calculer la date 3 ans après
    date_after = break_date_df + pd.DateOffset(years=3)
    
    # retain only dates that are < last date
    int_break_arr = int_break_arr[date_after<last_date]

    return int_break_arr, time, ndvi_smooth