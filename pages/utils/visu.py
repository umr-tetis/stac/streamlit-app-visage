import folium
import rasterio as rio 
from folium.raster_layers import ImageOverlay
import numpy as np 
from pathlib import Path
import xarray as xr
import matplotlib
import matplotlib.pyplot as plt
from pyproj import Transformer, CRS
import pandas as pd 
import streamlit as st
from streamlit_folium import st_folium
import ast 
matplotlib.rcParams.update({'font.size': 18})
import branca

def min_max_norm_na(raster):
    raster = (raster - np.nanmin(raster)) / (np.nanmax(raster) - np.nanmin(raster))
    return raster

def min_max_norm(raster, min:int, max:int):
    raster = (raster - min) / (max - min)
    raster[raster>1]=1
    return raster

def get_pos(lat, lng):
    return lat, lng


class VisuSimple:
    def __init__(
        self, 
        path_ndvi: Path,
        ):
        self.ds_ndvi = xr.open_dataset(path_ndvi)

    def get_lat_lon(self, lon:float, lat:float):
        
        ds1_crs = CRS.from_wkt(self.ds_ndvi.spatial_ref.crs_wkt)
        transformer = Transformer.from_crs("EPSG:4326", ds1_crs, always_xy=True)
        
        # transform coordinates
        x, y = transformer.transform(lat, lon)

        return x,y
    
    def get_rasters(self):
                
        x_min = self.ds_ndvi.y.min().item()
        y_min = self.ds_ndvi.x.min().item()
        
        x_max = self.ds_ndvi.y.max().item()
        y_max = self.ds_ndvi.x.max().item()

        self.bbox = [(x_min, y_min), (x_max, y_max)]
        self.raster_crs = CRS.from_wkt(self.ds_ndvi.spatial_ref.crs_wkt)
                
        if not self.raster_crs.to_epsg() == 4326:
            st.error("data is not espg4326 .. rerun smoothing script")
        mean_raster = self.ds_ndvi["ndvi_smooth"].mean(dim='time', skipna=True)
        
        self.NDVI = min_max_norm_na(mean_raster.values)
    
    def folium_map(self):
        map_cont = st.container(height=735, border=True)
        with map_cont:
            
            coordinates = st.text_input("Enter the coordinates (separated by a comma) LAT,LONG or click on the map", "")
            
            self.get_rasters()
            
            user_map = folium.Map(location=[self.bbox[1][0], self.bbox[0][0]])
            user_map.add_child(folium.ClickForMarker())
            folium.Rectangle(bounds=self.bbox, color='red', fill=False, fill_color='red', fill_opacity=0).add_to(user_map)
            user_map.fit_bounds(self.bbox)

            tile = folium.TileLayer(
                    tiles = 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
                    attr = 'Esri',
                    name = 'Esri Satellite',
                    overlay = False,
                    control = True
                ).add_to(user_map)
            

            cm = matplotlib.colormaps['Blues']
            image_overlay = ImageOverlay(image=cm(self.NDVI),name="Mean Ndvi",bounds=self.bbox, opacity=1, interactive=True, cross_origin=False,zindex=5)
            image_overlay.add_to(user_map)

            folium.LayerControl().add_to(user_map)

            fig =  folium.Figure(height=600, width=700).add_child(user_map)
            map_ = st_folium(
                fig,
                key="folium_map",
                render=False)
           
        if coordinates or map_.get("last_clicked"):
            if coordinates:
                lat, lon = map(float, coordinates.split(','))
                
            elif map_.get("last_clicked"):
                data = get_pos(map_["last_clicked"]["lat"], map_["last_clicked"]["lng"])
                
                if data is not None:
                    lat = data[0]
                    lon = data[1]
    
            time_value =  pd.to_datetime(self.ds_ndvi.time.values)
            
            pixel_data = self.ds_ndvi.sel(x=lon, y=lat, method="nearest")
            
            ndvi = pixel_data["250m_16_days_NDVI"]
            smooth = pixel_data["ndvi_smooth"]
            
            fig = plt.figure(figsize=(15, 10))
            gs = fig.add_gridspec(2, 1, height_ratios=[4, 1])
            ax1 = fig.add_subplot(gs[0])
            ax1.plot(pd.to_datetime(ndvi["time"]), ndvi.values/10000, color="silver",label="raw ndvi",linewidth=1.25)
            ax1.plot(pd.to_datetime(smooth["time"]), smooth.values/10000, color="green",label="smoothed ndvi" ,linewidth=2.1)
            
            date_range = pd.date_range(start=time_value[0], end=time_value[-1], freq='3MS')
            ax1.set_xticks(date_range, minor=True)
        

            st.pyplot(fig)


class Visu:
    def __init__(
        self, 
        path_stack: Path,
        path_ndvi: Path,
        path_list_csv: Path
        ):
        
        self.path_stack = Path(path_stack)
        self.ds_ndvi = xr.open_dataset(path_ndvi)
        self.path_list_csv = path_list_csv


    def get_index(self, lon:float, lat:float):
        
        # ds1_crs = CRS.from_wkt(self.raster_crs.to_wkt())
        # transformer = Transformer.from_crs("EPSG:4326", ds1_crs, always_xy=True)
        
        # # transform coordinates
        # lon, lat = transformer.transform(lon, lat)
        
        with rio.open(self.path_stack) as ds:
            row, col = ds.index(lon, lat)
            return row, col
        
    def get_lat_lon(self, lon:float, lat:float):
        
        ds1_crs = CRS.from_wkt(self.ds_ndvi.spatial_ref.crs_wkt)
        transformer = Transformer.from_crs("EPSG:4326", ds1_crs, always_xy=True)
        
        # transform coordinates
        x, y = transformer.transform(lat, lon)
        if type(self.ds_ndvi.coords['x'].values[0]) != type(x):
            if type(self.ds_ndvi.coords['x'].values[0]) == np.int32:
                x = np.int32(x)
                y = np.int32(y)
            else:
                print("error")  
        return x,y
    
    def get_rasters(self):
        
        with rio.open(self.path_stack) as src:
            # Get the bounds of the raster (left, bottom, right, top)
            bounds = src.bounds
            x1,y1,x2,y2 = bounds
            
            self.bbox = [(bounds.bottom, bounds.left), (bounds.top, bounds.right)] 
            self.raster_crs = src.crs
            
            # Read the data as an image (assuming 3 bands, e.g., RGB)
            self.procruste = min_max_norm(src.read(3),min=0,max=80)  
            
            #self.procruste = min_max_norm(src.read(3))
            self.procruste = self.procruste
            
            ndviratio = src.read(2)
            # ndviratio[ndviratio==-3000] = np.nan 
            ndviratio =  min_max_norm(ndviratio,min=0,max=130)
            self.ndviratio = ndviratio
            
            self.magnitude = min_max_norm(src.read(1),min=0,max=3500)
            
            self.magnitude_pondere = src.read(4)
            #self.magnitude_pondere = min_max_norm(src.read(4))

            self.date = src.read(5)
            
            self.rgb = np.stack((self.magnitude,self.ndviratio,self.procruste), axis=-1)
            nan_mask = np.isnan(self.rgb)
            self.rgb[nan_mask.any(axis=-1)] = np.nan
            
            
    def get_breakpoints(self):
        
        # Use the converters when reading the CSV
        df = pd.read_csv(self.path_list_csv, converters={
            "liste_l2_dist": lambda x: x.replace("'","").replace("  ",",").replace(" ",",").replace("[,","[").replace(",,",",").replace("\n",""),
            'time_value': lambda x: x.replace("\n","").replace("' '","','")
        })
        self.df_breaks = df
        
        
    def folium_map(self):
        map_cont = st.container(height=735, border=True)
        with map_cont:
            
            coordinates = st.text_input("Enter the coordinates (separated by a comma) LAT,LONG or click on the map", "")
            
            self.get_rasters()
            self.get_breakpoints()
            
            user_map = folium.Map(location=[self.bbox[1][0], self.bbox[0][0]])
            user_map.add_child(folium.ClickForMarker())
            folium.Rectangle(bounds=self.bbox, color='red', fill=False, fill_color='red', fill_opacity=0).add_to(user_map)
            user_map.fit_bounds(self.bbox)

            tile = folium.TileLayer(
                    tiles = 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
                    attr = 'Esri',
                    name = 'Esri Satellite',
                    overlay = False,
                    control = True
                ).add_to(user_map)
            

            image_overlay = ImageOverlay(image=np.uint8(self.rgb*255), name="rgb",bounds=self.bbox, opacity=1, interactive=True, cross_origin=False,zindex=6)
            image_overlay.add_to(user_map)

            cm = matplotlib.colormaps['Blues']
            image_overlay = ImageOverlay(image=cm(self.procruste),name="Dissimilarity",bounds=self.bbox, opacity=1, interactive=True, cross_origin=False,zindex=5)
            image_overlay.add_to(user_map)

            cm = matplotlib.colormaps['Greens']
            image_overlay = ImageOverlay(image=cm(self.ndviratio), name="NDVI ratio",bounds=self.bbox, opacity=1, interactive=True, cross_origin=False,zindex=4)
            image_overlay.add_to(user_map)

            
            cm = matplotlib.colormaps['Reds']
            image_overlay = ImageOverlay(image=cm(self.magnitude), name="Magnitude",bounds=self.bbox, opacity=1, interactive=True, cross_origin=False,zindex=3)
            image_overlay.add_to(user_map)

            cm = matplotlib.colormaps['Greys']
            image_overlay = ImageOverlay(image=cm(self.magnitude_pondere), name="Weighted Magnitude",bounds=self.bbox, opacity=1, interactive=True, cross_origin=False,zindex=2)
            image_overlay.add_to(user_map)

            # https://python-visualization.github.io/folium/latest/advanced_guide/colormaps.html#Built-in
            
            cm = plt.cm.get_cmap('Set1')
            cm = plt.cm.get_cmap('RdYlGn')
            date = self.date
            linear = branca.colormap.linear.RdYlGn_11.scale(np.min(date), np.max(date))
            #linear = branca.colormap.linear.Set1_09.scale(np.min(date), np.max(date))
            colorscale = linear.to_step(index=np.unique(date).tolist())
            
            colorscale.caption = 'Date of changes'
            colorscale.add_to(user_map)
            date = (date - np.min(date)) / (np.max(date) - np.min(date))
            image_overlay = ImageOverlay(image=cm(date), name="Date",bounds=self.bbox, opacity=1, interactive=True, cross_origin=False,zindex=1) # ,colormap = lambda x: colorscale(x)
            image_overlay.add_to(user_map)

            folium.LayerControl().add_to(user_map)
            
            fig =  folium.Figure(height=600, width=700).add_child(user_map)
            map_ = st_folium(
                fig,
                key="folium_map",
                render=False)
           
        if coordinates or map_.get("last_clicked"):
            if coordinates:
                lat, lon = map(float, coordinates.split(','))
                
            
            elif map_.get("last_clicked"):
                data = get_pos(map_["last_clicked"]["lat"], map_["last_clicked"]["lng"])
        
                if data is not None:
                    lat = data[0]
                    lon = data[1]
        
            x_in, y_in = self.get_index(lon=lon, lat=lat)
            df = self.df_breaks
            
            df_in = df[(df["pixel_y"] == x_in) & (df["pixel_x"] == y_in)] # to do we change here
            
            try:
                string = df_in["liste_l2_dist"].values[0]
                string = string.replace(",,", ",")
                #liste_l2_dist = ast.literal_eval(df_in["liste_l2_dist"].values[0])
                liste_l2_dist = ast.literal_eval(string) # ou eval()
            except IndexError:
                st.error('Point is out of region', icon="🚨")
            
            time_value = pd.to_datetime(ast.literal_eval(df_in["time_value"].values[0]))
            
            pixel_data = self.ds_ndvi.sel(x=lon, y=lat, method="nearest")
            
            ndvi = pixel_data["250m_16_days_NDVI"]
            smooth = pixel_data["ndvi_smooth"]
            
            fig = plt.figure(figsize=(15, 10))
            gs = fig.add_gridspec(2, 1, height_ratios=[4, 1])
            ax1 = fig.add_subplot(gs[0])
            ax1.plot(pd.to_datetime(ndvi["time"]), ndvi.values/10000, color="silver",label="raw ndvi",linewidth=1.25)
            ax1.plot(pd.to_datetime(smooth["time"]), smooth.values/10000, color="green",label="smoothed ndvi" ,linewidth=2.1)
            
            date_range = pd.date_range(start=time_value[0], end=time_value[-1], freq='3MS')
            ax1.set_xticks(date_range, minor=True)
            
            if len(liste_l2_dist) != len(time_value):
                st.error("Data error please run Again Change Detection", icon="🚨")
            
            # Ajouter une ligne verticale à chaque indice de date spécifié dans la liste
            for i, index_to_highlight in enumerate(liste_l2_dist):
                line_style = '--'
                line_width = 0.8
                if i == np.argmax(liste_l2_dist):
                    line_width = 2.0  # Épaissir le trait pour l'indice maximum
                    ax1.axvline(x=time_value[i], color='red', linestyle=line_style, linewidth=line_width, label="selected breakpoint")
                elif i == np.argmin(liste_l2_dist):
                    ax1.axvline(x=time_value[i], color='red', linestyle=line_style, linewidth=line_width, label="breakpoints")
                else:
                    ax1.axvline(x=time_value[i], color='red', linestyle=line_style, linewidth=line_width)

            ax1.grid(True)
            ax1.axis(ymin=0,ymax=1) 
            ax1.legend()
            ax1.set_title(f"Visualization of breakpoints for lat: {lat:.2f} lon: {lon:.2f}")
            
            ax2 = fig.add_subplot(gs[1], sharex=ax1)  # Utilise le même axe x que la première figure   
            # Créer une liste de couleurs pour chaque point dans le stem, en bleu sauf pour le point le plus élevé
            stem_colors = ['r' if i == np.argmax(liste_l2_dist) else 'b' for i in range(len(liste_l2_dist))]
            # Tracer le stem avec des couleurs spécifiées            

            stem = ax2.stem(time_value, liste_l2_dist, linefmt='-', markerfmt='o', basefmt='-', bottom=0, use_line_collection=True)
            stem[0].set_color('blue')  # Couleur de la tige en bleu
            stem[1].set_color(stem_colors)  # Couleur des points spécifiée par stem_colors
            # Ajouter une légende pour indiquer le point le plus élevé
            ax2.legend()
            ax2.set_xlabel('magnitude (L2 distance) of the breakpoints')
            ax2.axis(ymin=0,ymax=15000) 
            ax2.grid(True)
            plt.tight_layout()

            st.pyplot(fig)
            
                