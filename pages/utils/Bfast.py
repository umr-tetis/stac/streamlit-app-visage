import numpy as np
from dateutil.relativedelta import relativedelta
from bfast import BFASTMonitor
import logging
from datetime import timedelta
import pandas as pd
import streamlit as st
from stqdm import stqdm

logger = logging.getLogger(__name__)


class Bfast:
    def __init__(
        self,
        hfrac: int,
        freq: int,
        monitorFreq: int,
        historic_period: int = None,
        dateCorr: int = 1,
        nanvalue: float =-32768 
    ):
        self.hfrac = hfrac
        self.freq = freq
        self.monitorFreq = monitorFreq
        self.historic_period = historic_period
        self.dateCorr = dateCorr
        self.nan_value = nanvalue
        
        if np.isnan(self.historic_period):
            self.historic_period = 0
            logger.info("Bfast intialized with no historic period")

    def run_bfastMonitor(self, raster: np.ndarray, list_dates: list):
        # Parameters
        # raster: ndarray of shape (N, W, H),
        # where N is the number of time series points per pixel and W and H the width and the height of the image, respectively.

        # list_dates of datetime objects
        # Specifies the dates of the elements in data indexed by the first axis n_chunks : int or None, default None
        
        self.__get_duration(list_dates=list_dates)
        logger.info(f"running Bfast for historic_period = {self.historic_period}")

        raster[np.isnan(raster)] = self.nan_value
        raster = raster.astype("int16")

        start_date = list_dates[0] + relativedelta(years=3)
        end_date = list_dates[-1]

        monitorDates_list = pd.date_range(
            start_date, end_date - timedelta(days=1), freq=f"{self.monitorFreq}MS"
        )

        st.write(
            f"Monitoring Bfast with {len(monitorDates_list)} dates from {monitorDates_list[0].strftime('%Y-%m-%d')}"
            f" to {monitorDates_list[-1].strftime('%Y-%m-%d')} (every {self.monitorFreq} month)"
        )

        allChanges = []
        allMeans = []
        allMagnitudes = []

        for start_monitorDate in stqdm(monitorDates_list):

            start_monitorDate = start_monitorDate.to_pydatetime()
            start_hist = start_monitorDate - relativedelta(years=self.historic_period)
            
            # get date of beginning
            posHist = np.searchsorted(list_dates, start_hist, side='left') if np.any(np.array(list_dates) >= start_hist) else 0
            
            raster_cropped = raster[posHist:, :, :]
            list_dates_cropped = list_dates[posHist:]
            
            n_history = round((start_monitorDate - start_hist).days / self.freq)
            pos = np.where(np.array(list_dates_cropped) >= start_monitorDate)[0][0] + posHist 
            

            model = BFASTMonitor(
                start_monitor = start_monitorDate,  #A datetime object specifying the start of the monitoring phase.
                freq=self.freq,  # The frequency for the seasonal model.
                k=3,  # The number of harmonic terms.
                hfrac=self.hfrac,  # Float in the interval (0,1) specifying the bandwidth relative to the sample size in the MOSUM/ME monitoring processes.
                trend=True,  # Whether a tend offset term shall be used or not.
                level=0.05,  # Significance level of the monitoring (and ROC, if selected) procedure, i.e., probability of type I error.
                backend="python-mp",  # Le moteur de calcul utilisé ('opencl' utilise OpenCL, 'cpu' utilise le CPU , python-mp)
                device_id=0,  # Only relevant if backend=’opencl’. Specified the OpenCL device id
                detailed_results=False,  # Indique si les résultats détaillés doivent être générés
            )

            try:
                model.fit(data=raster_cropped, dates=list_dates_cropped, n_chunks=1, nan_value=self.nan_value)
            except:
                st.text("Error ..")

            datesMat = self.__filter_breaks(breaks = model.breaks, pos = pos, n_history = n_history)
            
            ## Property means
            ## Returns the means computed of the individual MOSUM processes (e.g., a positive mean for a pixel corresponds to an increase of the vegetation
            #  in case indices such as NDMI are considered)

            ## Property magnitudes
            ## Returns the magnitudes computed:values median of the difference between the data and the model prediction in the monitoring period
            # param=model.get_params()

            allChanges.append(datesMat)
            allMeans.append(model.means)
            allMagnitudes.append(model.magnitudes)

        allChanges = np.dstack(allChanges)
        allMeans = np.dstack(allMeans)
        allMagnitudes = np.dstack(allMagnitudes)

        return allChanges, allMeans, allMagnitudes

    def __get_duration(self, list_dates: list):

        # sanity check
        total_duration = list_dates[-1] - list_dates[0]

        months = total_duration.days // 30
        years, months = divmod(months, 12)

        if years > 6:
            print(f"We have data for {years} years and {months} months")
        else:
            print(
                f"We have data for {years} years and {months} months this is not enough. Skipping"
            )
            
    def __filter_breaks(self, breaks:np.ndarray, pos: int, n_history: int):
        
        # -2 corresponds to not enough data for a pixel
        # -1 corresponds to "no breaks detected"
        # idx with isx>=0 corresponds to the position of the first break

        breaks = breaks.astype(float)
        breaks[np.where(breaks < 0)] = np.nan

        if self.dateCorr == 1:
            breaks = breaks + pos - (round(self.hfrac * n_history))
        elif self.dateCorr == 2:
            breaks = breaks + pos  - (round(self.hfrac * n_history) / 2)
        else:
            breaks = breaks + pos 
        
        return breaks