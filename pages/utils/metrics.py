
import numpy as np
import xarray as xr
import streamlit as st
import matplotlib.pyplot as plt
from rasterio.crs import CRS
from stqdm import stqdm
from pages.utils.util import calculate_ndviratio_magni_pond_disparity, calculate_l2_distance, process_pixel_changes
from pathlib import Path
import pandas as pd

def min_max_norm(raster):
    return (raster - np.nanmin(raster)) / (np.nanmax(raster) - np.nanmin(raster))

class Mapping:
    def __init__(
        self, 
        dataset: xr.DataArray,
        changes: np.array
        ):
        self.dataset = dataset
        self.changes = changes 
        # dataset is the original dataset
        # changes are the changes detected
    
    def get_size(self):
        taille_x, taille_y = self.dataset.dims["x"], self.dataset.dims["y"]
        return taille_x, taille_y
    
    def get_empty_image(self):
        return xr.DataArray(np.zeros((self.dataset.dims["x"], self.dataset.dims["y"])), dims=['x', 'y'])
    
    def process_one_pixel(x:int, y:int):
        return np.nan

 
    def map(dataset: xr.DataArray, filtered_data: xr.DataArray,name: str, changes: np.array= None):
        print("Creation des cartes")

        if changes is None:
            return

        taille_x, taille_y = dataset.dims["x"], dataset.dims["y"]
        # image_vide_date, image_vide_procruste, image_vide_ndviratio, image_vide_magnitude, image_vide_pondere = [xr.DataArray(np.zeros((taille_x, taille_y)), dims=['x', 'y']) for _ in range(5)]

        variables = ["magnitude", "ndviratio", "procruste", "magnitude_pondere", "date"]
        # image_vide = xr.merge([xr.DataArray(np.zeros((taille_x, taille_y)), dims=['x', 'y'],name=name) for name in variables])
        image_vide = xr.combine_by_coords([xr.DataArray(np.zeros((taille_x, taille_y)), dims=['x', 'y'],name=name) for name in variables])
        
        # Obtenir les coordonnées de tous les pixels
        #coords = np.indices((taille_x, taille_y)).reshape(2, -1).T
        min_date = int(str(dataset.time.values.min())[:4])
        data_dist = []
        
        st.write("Generate map (this can take a while) ...")
        for pixel_x in stqdm(range(taille_x)):
            for pixel_y in range(taille_y):

                int_break_arr, time, ndvi_smooth = process_pixel_changes(filtered_data, changes, pixel_x, pixel_y)
                
                if len(int_break_arr) == 0:
                    image_vide['date'].loc[{'y': pixel_y ,'x': pixel_x}] = min_date
                    image_vide['ndviratio'].loc[{'y': pixel_y, 'x': pixel_x}] = 0
                else:
                    # take 0.14 second 
                    liste_l2_dist, breakpointmax = calculate_l2_distance(time, ndvi_smooth, int_break_arr)
                    data_dist.append([pixel_y, pixel_x, liste_l2_dist, time.values[int_break_arr]])
                    
                    # take 0.06 second
                    premiere_image = time.values[breakpointmax]

                    meanNDVI_absLogRatio, magni_pondere_abs_result, procruste = calculate_ndviratio_magni_pond_disparity(time, ndvi_smooth, breakpointmax)
                    magni = liste_l2_dist[-1] # why we take the last one ? 
                    magni_pondere_abs_result = magni_pondere_abs_result + procruste
                    
                    image_vide['magnitude_pondere'].loc[{'y': pixel_y, 'x': pixel_x}] = magni_pondere_abs_result
                    image_vide['date'].loc[{'y': pixel_y, 'x': pixel_x}] = int(str(premiere_image)[:4])
                    image_vide['procruste'].loc[{'y': pixel_y ,'x': pixel_x}] = procruste*100
                    image_vide['ndviratio'].loc[{'y': pixel_y, 'x': pixel_x}] = meanNDVI_absLogRatio*100
                    image_vide['magnitude'].loc[{'y': pixel_y, 'x': pixel_x}] = magni
                                

        dataset = dataset.assign(date=image_vide['date'])
        dataset = dataset.assign(magnitude_pondere=image_vide['magnitude_pondere'])
        dataset = dataset.assign(procruste=image_vide['procruste'])
        dataset = dataset.assign(ndviratio=image_vide['ndviratio'])
        dataset = dataset.assign(magnitude=image_vide['magnitude'])
        
        ds = dataset[variables]
        # get crs info
        crs_wkt = dataset.spatial_ref.attrs.get('crs_wkt', None)
        ds = ds.rio.write_crs(crs_wkt, inplace=True)
        ds = ds.rio.set_spatial_dims(x_dim="x", y_dim="y", inplace=True)
        
        ds = ds.transpose('y', 'x').rio.reproject('EPSG:4326', nodata=np.nan)
        
        out_name = f'pages/data/{name[2:]}_stack.tif'
        out_name_csv = f'pages/data/{name[2:]}_list.csv'
        
        ds.rio.to_raster(out_name)
        # Convert the list to a pandas DataFrame
        df = pd.DataFrame(data_dist, columns=['pixel_y', 'pixel_x','liste_l2_dist', 'time_value'])
        
        # Save the DataFrame to a CSV file
        df.to_csv(out_name_csv, index=False)
        st.success(f"Images {Path(out_name).stem} stack is saved with crs {CRS.from_wkt(dataset.spatial_ref.crs_wkt)}")