import os
import streamlit as st
import xarray as xr
import numpy as np
import warnings
warnings.filterwarnings("ignore")
import multiprocessing
import matplotlib.pyplot as plt

def replace_values(arr:np.array,value:int):
    arr = np.array(arr, dtype=np.float64)  # Convert list to NumPy array with float type
    
    # Find indices where values are greater than 1
    greater_than_one = np.where(arr > 1)[0]
    
    # Iterate over the indices with values greater than 1
    for idx in greater_than_one:
        # Find the previous valid value (not NaN and <= 1)
        prev_idx = idx - 1
        while prev_idx >= 0 and (np.isnan(arr[prev_idx]) or arr[prev_idx] > value):
            prev_idx -= 1
        
        # Find the next valid value (not NaN and <= 1)
        next_idx = idx + 1
        while next_idx < len(arr) and (np.isnan(arr[next_idx]) or arr[next_idx] > value):
            next_idx += 1
        
        # If valid values are found before and after, replace the value
        if prev_idx >= 0 and next_idx < len(arr):
            arr[idx] = np.mean([arr[prev_idx], arr[next_idx]])
        else:
            arr[idx] = value
    
    return arr

def lister_fichiers_dossier(dossier_path):
    fichiers = []
    with os.scandir(dossier_path) as entries:
        for entry in entries:
            if entry.is_file() and entry.name.startswith("1_"):
                fichiers.append(entry.name)
    return fichiers


def preprocess_lissage(data):
    # Ouvrir le fichier NetCDF en tant que dataset Xarray

    ##preparation data
    data['diff_time'] = (data['time'].diff('time').dt.days ).astype(int) # On calcule la différence temporelle entre les valeurs de temps

    #data = data.dropna(dim='time')  # On supprime les lignes qui contiennent des valeurs manquantes

    data['x'] = (data['x'] ).astype(int)
    data['y'] = (data['y']).astype(int)

    # Appliquer cumsum pour chaque pixel
    data['lag'] = data['diff_time'].cumsum(dim='time')
    
    # Defini les weigth selon le papier
    # WL = (1- (lag/25)³)³
    ### CAlculer dans la fenetre
    data['wl'] = 0

    # WQ Quality pixel reliability
    data['wq'] = 0
    data['wq'] = xr.where(data['250m_16_days_pixel_reliability'] == 0, 1, data['wq'])
    data['wq'] = xr.where(data['250m_16_days_pixel_reliability'] == 1, 0.5, data['wq'])
    data['wq'] = xr.where((data['250m_16_days_pixel_reliability'] != 1) & (data['250m_16_days_pixel_reliability'] != 0), 0, data['wq'])

    # WZ VIEW ZENITH ANGLE
    data['250m_16_days_view_zenith_angle'] = data['250m_16_days_view_zenith_angle']  *  0.01

    # data['wz'] = 1
    # data['wz'] = xr.where((data['250m_16_days_view_zenith_angle'] < 35) & (data['250m_16_days_view_zenith_angle'] > 20), 0.5, data['wz'])
    # data['wz'] = xr.where(data['250m_16_days_view_zenith_angle'] > 35, 0, data['wz'])
    # WR = WQ x WZ
    data['wr'] = data['wq'] #* data['wz']

    # W = WR x WL
    # Recast pour gagner de la mémoire
    data['wq'] = data['wq'].astype(np.float16)
    #data['wz'] = data['wz'].astype(np.float16)
    data['wr'] = data['wr'].astype(np.float16)

    # Supprimer les variables inutiles pour libérer de la mémoire
    data = data.drop_vars(['diff_time', '250m_16_days_pixel_reliability', '250m_16_days_view_zenith_angle','wl','wq'])


    # Créer un masque où les valeurs négatives sont True et les autres False
    mask = data['250m_16_days_NDVI'] < 0

    # Utiliser xarray.where() pour convertir les valeurs négatives en zéro pour eviter les soucis lors du lissage
    data['250m_16_days_NDVI'] = xr.where(mask, 1, data['250m_16_days_NDVI'])

    return data   


def lissage( ds  , window , order , data_to_smooth  , data_smooth, compteur = 0 ) :
    
    window_supp = int(window / 2) + 1
    window_inf = int(window / 2) 
    wl=[0]*window
    w = [0] * window 
    weigth = [0] * len(ds.time)
    lisse = [np.nan] * len(ds.time)
    
    ##BOUCLE 1 : parcours de la serie temporel
    ##ON SELECTIONNE DES LISTE DE LA TAILLE DE LA WINDOW
    for i in range( window_inf, len(ds.time)-window_supp):

        y= ds[data_to_smooth].values[i-window_inf:i+window_supp].reshape(window)
        x= ds["lag"].values[i-window_inf:i+window_supp].round()
        wr= ds["wr"].values[i-window_inf:i+window_supp]

        ##BOUCLE 2 : calcul des poids selon la window
        ## DEFINI LE LAG WL et calcul W
        for j in range (0,window):
            lag_ = abs(j-window_inf)
            wl[j]= (1 - ( lag_ / 25) ** 3) ** 3
            w[j] = wl[j]*wr[j] 
            
        # convert to 1D array
        w = np.ravel(w)
        weigth[i+window_supp] = w[window_supp]
        try:
        ## ON CALCULE LES COEFF AVEC POLYFIT SELON LES POIDS
            coefficients = np.polyfit(x, y, order, w=w)        
            ## ON CALCUL NOTRE POINT SMOOTH 
            y_smooth = np.polyval(coefficients, x[window_supp])
            #print(y_smooth)
            #print(w[window_supp])
            lisse[i] = y_smooth

        except Exception as e:  
            compteur = compteur+1
            lisse[i]=0
              
    lisse = np.array(lisse) 
    lisse = replace_values(arr=lisse, value=10000)
    
    ds['weight'] = xr.DataArray(weigth, dims='time')
    ds[data_smooth] = xr.DataArray(lisse, dims='time')
    
    return ds 


def all_pix ( data_subset, window_selected , order: int, iteration: int):
    
    for it_ in range(0,iteration):
        #save the different round of iteration for smoothing
        data_subset[f"ndvi_smooth_{it_}"] = xr.DataArray(np.zeros_like(data_subset["250m_16_days_NDVI"].values), dims=data_subset["250m_16_days_NDVI"].dims, coords=data_subset["250m_16_days_NDVI"].coords)
    data_subset[f"ndvi_smooth"] = xr.DataArray(np.zeros_like(data_subset["250m_16_days_NDVI"].values), dims=data_subset["250m_16_days_NDVI"].dims, coords=data_subset["250m_16_days_NDVI"].coords)
        
    
    data_subset["weight"] = xr.DataArray(np.zeros_like(data_subset["250m_16_days_NDVI"].values), dims=data_subset["250m_16_days_NDVI"].dims, coords=data_subset["250m_16_days_NDVI"].coords)

    for i in range(data_subset.dims["y"]):
        #print(i , "/" , data_subset.dims["y"])
        for j in range(data_subset.dims["x"]):

            
            # sélectionner les données pour un seul pixel à la fois
            pixel_data = data_subset.isel(y=i, x=j)
            #a = np.array(pixel_data.x)
            #b = np.array(pixel_data.y)
            #print(a,b)

            # appliquer votre traitement à chaque tranche de temps pour le pixel sélectionné
            # ici, nous supposons que votre traitement est une fonction appelée "mon_traitement"
            if iteration > 0:
                pixel_data_traite = pixel_data
                pixel_data_traite = lissage( pixel_data  , window = window_selected , order = order, data_to_smooth =  "250m_16_days_NDVI" , data_smooth = "ndvi_smooth_0",compteur = 0 )
                
                
                for it_ in range(1,iteration):
                    pixel_data_traite = lissage( pixel_data_traite  , window = window_selected , order = order, data_to_smooth =  f"ndvi_smooth_{it_ -1}" , data_smooth = f"ndvi_smooth_{it_}",compteur = 0 )
                    data_subset[f"ndvi_smooth_{it_ -1}"].loc[{"time": pixel_data.time, "y": pixel_data.y, "x": pixel_data.x}] = pixel_data_traite[f"ndvi_smooth_{iteration-1}"]
                # Extraire la variable contenant les données du pixel
                pixel_variable = pixel_data_traite[f"ndvi_smooth_{iteration-1}"]
            else:
                pixel_variable = pixel_data["250m_16_days_NDVI"]
                
            # remplacer les données originales dans le jeu de données par les données traitées
            data_subset["ndvi_smooth"].loc[{"time": pixel_data.time, "y": pixel_data.y, "x": pixel_data.x}] = pixel_variable
    
    return data_subset


def split_data(data):
    list_x = data.x.values
    list_y = data.y.values

    mid_x = int(len(list_x) / 2)
    mid_y = int(len(list_y) / 2)

    print(len(list_x))
    print(len(list_y))

    # left bot
    part_1 = data.sel(x=slice(list_x[0], list_x[mid_x]),
                      y=slice(list_y[0], list_y[mid_y]))
    # right bot
    part_2 = data.sel(x=slice(list_x[mid_x], list_x[len(list_x) - 1]),
                      y=slice(list_y[0], list_y[mid_y]))
    # left top
    part_3 = data.sel(x=slice(list_x[0], list_x[mid_x]),
                      y=slice(list_y[mid_y], list_y[len(list_y) - 1]))
    # right top
    part_4 = data.sel(x=slice(list_x[mid_x], list_x[len(list_x) - 1]),
                      y=slice(list_y[mid_y], list_y[len(list_y) - 1]))

    return part_1, part_2, part_3, part_4


# def save_to_netcdf(ds_results, chemin_fichier):
#     # Sauvegarder le dataset dans un fichier NetCDF
#     ds_results.to_netcdf(chemin_fichier)
#     st.write("Fichier NetCDF exporté !")


def display_first_image(ds,origin):
    # Extraire la première tranche de temps
    first_time_slice = ds.isel(time=0)
    # Extraire la variable pour l'afficher avec Matplotlib
    variable = first_time_slice['250m_16_days_NDVI']
    # Créer une figure Matplotlib
    fig, ax = plt.subplots()
    ax.imshow(variable, cmap="viridis", origin=origin)
    ax.axis('off')  # Supprimer les axes
    st.write('Image date : ',ds.time[0].values)
    # Afficher la figure dans Streamlit
    st.pyplot(fig)

def main():
    st.title("Why smoothing?")
    st.write("MODIS NDVI time series can be affected by noise due to the presence of clouds, shadows, etc. In this section, the NDVI time series are smoothed using weighted Savitsky-Golay algorithm, meaning that the smoothing is weigthed using quality data (here: reliability and view zenith angle associated to each NDVI value in the MOD13Q1 product). Tuning parameters are available to adapt the smoothing to the quality of the raw NDVI time series (highly dependent to climate), and recommendations are given for their setting. For more details concerning the weighted Savitsky-Golay smoothing algorithm, see Piou et al., (2013).")
    st.write("**References**")
    st.write("PIOU, Cyril, LEBOURGEOIS, Valentine, BENAHI, Ahmed Salem, BONNAL, Vincent, EL HACEN JAVAAR, Mohamed, LECOQ, Michel, & VASSAL, Jean Michel (2013). Coupling historical prospection data and a remotely-sensed vegetation index for the preventative control of Desert locusts. Basic and Applied Ecology, 14(7), 593-604.")

    st.title("My time series:")
    # Sélection du dossier
    dossier_path = "pages/data"
    # Liste des fichiers dans le dossier filtrés par ceux qui commencent par "1_"
    fichiers_dans_dossier = lister_fichiers_dossier(dossier_path)
    # Sélection du fichier
    fichier_selectionne = st.selectbox("Select your MODIS NDVI time series:", fichiers_dans_dossier)
    st.write("Yo have selected:", fichier_selectionne[2:-3])
    save_file_name = fichier_selectionne[2:-3]

    data = xr.open_dataset(f'pages/data/{fichier_selectionne}')
    st.write(data.dims)
    display_first_image(data,"upper")
    
    st.title("Smoothing configuration & running")
    st.write("Savitsky-Golay filtering function aims at fitting a polynomial of a certain order to groups of sample points. The number of sampling points considered in a group is defined by the Window Size parameter while Filter Order controls the order of the used polynomial. The central point of each group is replaced by the value predicted at the same position by the fitted polynomial.")
    st.write("**Smoothing window size**")
    st.write("A larger Window Size results in more sampling points being considered in the fitting process, hence a smoother curve. As the length of the window increases, the estimation variance decreases, but the bias error increases at the same time. The window size is expressed in number of points in the time series. Ex: a window size of 10 means a smoothing period of 160 days, as the frequency of MOD13Q1 product is 16 days.")


    window_selected = st.slider("Please select the smoothing window size (in number of observations). Recommendations can be done regarding the application environment : 7 for dry, 13 for semi-humid, and 15 for humid", 5 , 20, 13, 1)
    
    st.write("**Filter order**")
    st.write("A higher order allows for more detailed curves, but conversely can also preserves local noise.")
    order = st.slider("Please select the filter order (2-5):", 2, 5, 2)

    st.write("**Envelope iteration number**")
    st.write("The presence of clouds or noise in NDVI time series tends to have a negative impact on the value of the indice. Making one or more iterations aims to extract the upper envelope of the NDVI time series and have proven effective at eliminating the influence of low-value noise caused by clouds and shadows (e.g. if your study area is highly impacted by cloudy conditions).")
    iteration = st.slider("Please select the number of envelope iterations (1-3, or 0 for no iterations):", 0, 3, 1)
    
    save_file_name = f"{save_file_name}-Window{window_selected}-Order{order}-Iteration{iteration}"
    

    # Ajouter un bouton "Choose default value (no iterations)"

    # Bouton pour appliquer la fonction de prétraitement sur le fichier
    if st.button("Run smoothing function"):
        
        # dans le warning en dessous, ajouter les valeurs de filter order et number of iterations
        st.warning(f"Filter in progress with window_selected : {window_selected} .. do not close your windows")
        data = preprocess_lissage(data)
        # Diviser les données en parties
        part1, part2, part3, part4 = split_data(data)
        partitions = 4
        
        # Créer une piscine de processus pour le traitement parallèle
        pool = multiprocessing.Pool(processes=partitions)
        # Créer une liste d'entrées pour chaque partie
        inputs = [part1, part2, part3, part4]
        
        # Appliquer la fonction all_pix sur chaque partie en parallèle
        results = pool.starmap(all_pix, [(data_subset, window_selected, order, iteration) for data_subset in inputs])        # Fusionner les résultats en un seul dataset
        ds_results = xr.merge(results)
        # Supprimer les variables inutiles pour libérer de la mémoire
        ds_results = ds_results.drop_vars(['wr'])
        st.success("Filtering is done")
        # Chemin de fichier où vous voulez sauvegarder le dataset
        chemin_fichier = f'pages/data/2_{save_file_name}.nc'
        # Sauvegarder le dataset dans un fichier NetCDF
        
        crs_wkt = ds_results.spatial_ref.attrs.get('crs_wkt', None)
        ds_results = ds_results.rio.write_crs(crs_wkt, inplace=True)
        ds_results = ds_results.rio.set_spatial_dims(x_dim="x", y_dim="y", inplace=True)
        ds_results = ds_results.rio.reproject('EPSG:4326', nodata=np.nan)
                
        ds_results.to_netcdf(chemin_fichier, format="NETCDF3_64BIT",engine="scipy")
        # Afficher la première image après l'export
        display_first_image(ds_results,"lower")



if __name__ == "__main__":
    main()