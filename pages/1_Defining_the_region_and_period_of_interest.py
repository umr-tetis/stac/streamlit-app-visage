
import folium
import subprocess
from datetime import datetime
import matplotlib.pyplot as plt
import odc.stac
import planetary_computer 
import pystac_client
import pystac
import logging
from dateutil.relativedelta import relativedelta
import geopandas as gpd
import streamlit as st
from streamlit_folium import folium_static
import xarray as xr
from streamlit_folium import st_folium
import numpy as np

logger = logging.getLogger(__name__)

def km_to_degrees_latitude(km):
    # 1 degree of latitude is approximately 111 kilometers
    return km / 111.0

def get_pos(lat, lng):
    return lat, lng

def carte_acquisition(): 
    coordinates = st.text_input("Enter the coordinates (separated by a comma) LAT,LONG or click on the map", "")
    # Créer une carte Folium
    my_map = folium.Map(location=[20, 20], zoom_start=1, control_scale=True)

    # Ajouter une fonction pour récupérer les coordonnées lorsqu'un clic se produit sur la carte
    my_map.add_child(folium.ClickForMarker())
    my_map.add_child(folium.ClickForMarker(popup='Point'))

    tile = folium.TileLayer(
            tiles = 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
            attr = 'Esri',
            name = 'Esri Satellite',
            overlay = False,
            control = True
        ).add_to(my_map)

    folium.LayerControl().add_to(my_map)
    fig =  folium.Figure(height=600, width=800).add_child(my_map)
    map_ = st_folium(
        fig,
        key="folium_map",
        render=False)
    
    if coordinates:
        lat_user, lon_user = map(float, coordinates.split(','))
        return lat_user, lon_user
    elif map_.get("last_clicked"):
        data = get_pos(map_["last_clicked"]["lat"], map_["last_clicked"]["lng"])
        
        coordinates = None
        if data is not None:
            lat = data[0]
            lon = data[1]
            return lat, lon
    else:
        return None, None
    # Afficher la carte avec Streamlit
    #folium_static(my_map)

    # Récupérer les coordonnées lorsqu'un clic se produit sur la carte
    st.info("Click on the map for lat long")

def carte_buffer(bbox):
    # Calculer la bbox
    # Créer une carte Folium
    my_map = folium.Map(location=[bbox[1], bbox[0]], zoom_start=5, control_scale=True)

    # Ajouter la bbox à la carte
    folium.Rectangle(bounds=[[bbox[1], bbox[0]], [bbox[3], bbox[2]]], color='red', fill=True, fill_color='red', fill_opacity=0.2).add_to(my_map)

    # Afficher la carte avec Streamlit
    folium_static(my_map)
    
def select_date_range():
    st.subheader("Defining the period of interest")
    st.write("NB. Modis NDVI time series are available from 2000/02/18")

    # Ajouter des widgets pour la sélection de la date de début et de fin
    start_date = st.date_input("Start date (YYYY/MM/DD):", datetime.today(), min_value=datetime.strptime("2000-01-01", "%Y-%m-%d").date())
    end_date = st.date_input("End date (YYYY/MM/DD):", datetime.today())

    if end_date > datetime.today().date():
        end_date = datetime.today().date()
        st.success(f"End date cannot be later than today's date. Selected time interval: from {start_date} to {end_date}")
    else:
        st.success(f"Selected time interval : from {start_date} to {end_date}")
        
    start_date = start_date - relativedelta(months=4)
    end_date = end_date + relativedelta(months=4)
    
    if end_date > datetime.today().date():
        end_date = datetime.today().date()
        
    st.success(f"Modified time interval: from {start_date} à {end_date}")
    return start_date, end_date  

def parametre():

    st.write("This component is dedicated to the definition of the study area and period of interest for downloading MODIS NDVI time series. The definition of the study  area can be done either by providing the coordinates (latitude, longitude) of its central point together with a buffer size (in km from the central point), or by uploading a geojson of your study zone. At the end, the script is run and the MODIS NDVI time series will be downloaded from Microsoft Planetary Computer.")
    st.title("Study area definition")

    option = st.radio("Choose an option:", ("Coordinates of the central point (lat/lon)", "GEOJSON"))

    if 'coordinates' not in st.session_state:
        st.session_state.coordinates = []

    #### MANUEL
    if option == "Coordinates of the central point (lat/lon)":
        # Barre de recherche pour les coordonnées 
        map_cont = st.container(height=735, border=True)
        with map_cont:
            lat, lon = carte_acquisition()
        
        if not lon:
            st.write("Click on the map to get coordinates (lat/lon)")  
        else:
            st.write("Coordinates of the central point (lat/lon)")  
            st.success(f"Valid coordinates: Latitude {lat:0.3f}, Longitude {lon:0.3f}")

             # Barre de sélection pour le buffer
            buffer_km = st.slider("Please provide the buffer size of your study area (in km):", 10, 100, 20, 10)
            if st.button:
                buffer = km_to_degrees_latitude(buffer_km)
                bbox = [lon - buffer, lat - buffer, lon + buffer, lat + buffer]
                carte_buffer(bbox)
                
            # Sélection de l'intervalle de temps
            start_date, end_date = select_date_range()
            
            # Champ de saisie pour le nom du fichier
            save_file_name = st.text_input("Enter the name of the output file (raw MODIS NDVI time series):", "output_file")
            
             # Bouton pour exécuter le script
            if st.button("Run script to download MODIS NDVI time series"):
                 bbox = [lon - buffer, lat - buffer, lon + buffer, lat + buffer] # bbox 
                 st.write(bbox)
                 run_script(bbox, start_date, end_date,save_file_name)
            
    #### GEOJSON
    elif option == "GEOJSON":
        st.write("Yoo choose option 2 GeoJson")
        uploaded_file =  st.file_uploader("Choose a geometry file", accept_multiple_files=False)
        if uploaded_file is not None:
            # data = json.loads(content)
            data = gpd.read_file(uploaded_file)
            
            # Vérifiez si le fichier GeoJSON contient des coordonnées
            try:
                min_lon, min_lat, max_lon, max_lat = data.total_bounds
                bbox  = [min_lon, min_lat, max_lon, max_lat]
                st.write("Extracted coordinates :", bbox)
                carte_buffer(bbox)
                # Sélection de l'intervalle de temps
                start_date, end_date = select_date_range()
                
                # Champ de saisie pour le nom du fichier
                save_file_name = st.text_input("Enter the name of the saved file", "output_file")
                
                # Bouton pour exécuter le script
                if st.button("Run script"):
                    try:
                        run_script(bbox, start_date, end_date,save_file_name) 
                    except:
                        st.write('error dl')
            except:
                st.write("GeoJSON file is incorrect")

def data_water(bbox):
    # Define time period (for ESRI Land Cover, the dataset is available for the year 2020)
    time_range = "2020-01-01/2020-12-31"
    
    catalog = pystac_client.Client.open(
        "https://planetarycomputer.microsoft.com/api/stac/v1",
        modifier=planetary_computer.sign_inplace)

    ##search in catalog
    search = catalog.search(collections=["esa-worldcover"], bbox=bbox, datetime=time_range)
    items = search.get_all_items()

    items_dict = dict()
    for i in range(len(items)):
        items_dict[i] = items[i]

    data = odc.stac.load(
        items_dict.values(),
        crs="EPSG:3857",
        bbox=bbox,
        resolution=250) 
    
    # some pre-processing
    data = data.drop_vars("input_quality.1")
    data = data.drop_vars("input_quality.2")
    data = data.drop_vars("input_quality.3")
    
    # True to put to NaN Data that is water
    data["map"] = data["map"]==80

    return data

def display_first_image(ds):
    # Extraire la première tranche de temps
    first_time_slice = ds.isel(time=int(len(ds.time)/2))

    # Extraire la variable pour l'afficher avec Matplotlib
    variable = first_time_slice['250m_16_days_NDVI']

    # Créer une figure Matplotlib
    fig, ax = plt.subplots()
    ax.imshow(variable, cmap="viridis", origin="upper")
    ax.axis('off')  # Supprimer les axes

    # Afficher la figure dans Streamlit
    st.pyplot(fig)

def run_script(bbox, start_date, end_date, save_file_name):
    try:
        st.warning("downloading NDVI .. do not close your windows")
        ds1 = data_ndvi_dl(bbox,start_date,end_date,save_file_name)
        st.success("NDVI downloaded")
        
        
        st.warning("downloading metadata .. do not close your windows")
        water_mask = data_water(bbox)
        ds1['250m_16_days_NDVI'] = xr.where(water_mask["map"].values, np.nan, ds1['250m_16_days_NDVI'])
        
        ds2 =data_other_dl(bbox,start_date,end_date,save_file_name)
        st.success("Metadata downloaded")
       
        st.warning("saving to netCDF ..")
        # Effectuer la jointure sur toutes les dimensions
        #ds_combined = ds1.combine_first(ds2)
        ds_combined = xr.combine_by_coords([ds2, ds1], combine_attrs = "identical")
         # Chemin de fichier où vous voulez sauvegarder le dataset
        chemin_fichier = f'pages/data/1_{save_file_name}.nc'
        # Sauvegarder le dataset dans un fichier NetCDF
        #ds_combined.to_netcdf(chemin_fichier)
        ds_combined.to_netcdf(chemin_fichier,format="NETCDF3_64BIT",engine="scipy")
        st.success("Script executed with success")
        display_first_image(ds_combined)
        st.write("Datacube shape :", ds_combined.dims)

    except subprocess.CalledProcessError as e:
        st.error(f"Error while executing the script : {e}")

def data_ndvi_dl(bbox,start_date,end_date,save_file_name):
   
    catalog = pystac_client.Client.open(
        "https://planetarycomputer.microsoft.com/api/stac/v1",
        modifier=planetary_computer.sign_inplace,
    )
    ## Parametre
    time_range = f'{start_date}/{end_date}'
         
    image_select = "MOD" #filtre collection 

    st.warning("search in catalog .. do not close your windows")
    ##search in catalog
    search = catalog.search(
    collections=["modis-13Q1-061"], bbox=bbox, datetime=time_range)
    items = search.get_all_items()
    ## Filter search
    mod_items = list(filter(lambda x: x.id.startswith(image_select), items))
    items = pystac.ItemCollection(mod_items)

    ### dl data
    items_dict = dict()
    for i in range(len(items)):
        items_dict[i] = items[i]

    st.warning("loading values .. do not close your windows")
    data = odc.stac.load(
        items_dict.values(),
        crs="EPSG:3857",
        bbox=bbox,
        resolution=250, ## per pixel
        bands=["250m_16_days_NDVI"],#bande to select
        nodata=-3000) 
    
    return data


def data_other_dl(bbox,start_date,end_date,save_file_name):
   
    catalog = pystac_client.Client.open(
        "https://planetarycomputer.microsoft.com/api/stac/v1",
        modifier=planetary_computer.sign_inplace,
    )
    ## Parametre
    time_range = f'{start_date}/{end_date}'
     
    image_select = "MOD" #filtre collection 

    ##search in catalog
    search = catalog.search(
    collections=["modis-13Q1-061"], bbox=bbox, datetime=time_range)
    items = search.get_all_items()
    ## Filter search
    mod_items = list(filter(lambda x: x.id.startswith(image_select), items))
    items = pystac.ItemCollection(mod_items)

    ### dl data
    items_dict = dict()
    for i in range(len(items)):
        items_dict[i] = items[i]

    data = odc.stac.load(
        items_dict.values(),
        crs="EPSG:3857",
        bbox=bbox,
        resolution=250, ## per pixel
        bands=["250m_16_days_pixel_reliability","250m_16_days_view_zenith_angle"],#bande to select
        nodata=72
        ) 
    
    return data
    # Chemin de fichier où vous voulez sauvegarder le dataset
    #chemin_fichier = f'pages/data/1_{save_file_name}.nc'

    # Sauvegarder le dataset dans un fichier NetCDF
    #data.to_netcdf(chemin_fichier)

def main():
    st.title("Download your MODIS NDVI time series")
    parametre()

if __name__ == "__main__":
    main()
    
