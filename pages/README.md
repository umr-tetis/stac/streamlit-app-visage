**VISAGE
DOCUMENTATION**


Ce fichier contient des instructions et des informations sur l'installation, l'exécution et les fonctionnalités de l'application VISAGE.
Environnement

    Python 3.8.15 

**Installation avec un environemment conda**
Requirements text

Installez les dépendances en utilisant l'outil de gestion de paquets Python, pip, avec le fichier requirements.txt fourni. Exécutez la commande suivante :

    pip install -r requirements.txt

Cette commande va installer toutes les dépendances répertoriées dans le fichier requirements.txt.
Dockerfile

Pour construire l'image Docker, ouvrez un terminal ou une invite de commande dans le répertoire contenant le Dockerfile, puis exécutez la commande suivante :


    docker build -t mon_image .

    ex :

    docker build -t visage .

Cela construira une image Docker à partir du Dockerfile dans le répertoire actuel. Vous pouvez spécifier un nom et éventuellement un tag pour votre image en utilisant l'option -t.

Pour lancer un conteneur à partir de cette image, utilisez la commande docker run. Par exemple :

    docker run -d --name <path data in local>:/app/pages/data mon_conteneur mon_image

    docker run --restart always -v /home/simon/Project/Visage/streamlit-app-visage/outputs:/app/pages/data -d -p 8501:8501 visage

**Application**
Lancer Application

Pour lancer l'application, exécutez l'une des commandes suivantes :

    streamlit run Home.py
ou
    bash

    python3 -m streamlit run Home.py

Les pages
Home.py
Fonctionnalités

    Page d'accueil.

Fonctions

    Aucune

Selectionner_la_zone.py
Fonctionnalités

    Sélection manuelle des coordonnées géographiques ou import d'un fichier GEOJSON pour définir la région d'intérêt.
    Sélection de l'intervalle de temps pour la récupération des données.
    Affichage d'une carte Folium pour visualiser la région d'intérêt et la bbox.
    Téléchargement des données à partir d'un catalogue STAC à l'aide de l'API Planetary Computer.
    Traitement des données téléchargées.
    Affichage des premières tranches temporelles des données téléchargées avec Matplotlib.

Fonctions

    carte_acquisition()
        Description : Crée une carte Folium pour permettre à l'utilisateur de sélectionner manuellement des coordonnées géographiques en cliquant sur la carte.
        Entrée : Aucune.
        Sortie : Aucune.

    carte_buffer(bbox)
        Description : Crée une carte Folium avec une bbox donnée pour afficher la zone d'intérêt avec un buffer.
        Entrée : bbox (list) - Les coordonnées de la bounding box au format [min_lon, min_lat, max_lon, max_lat].
        Sortie : Aucune.

    select_date_range()
        Description : Affiche des widgets pour sélectionner une plage de dates.
        Entrée : Aucune.
        Sortie : start_date (datetime) - Date de début sélectionnée par l'utilisateur, end_date (datetime) - Date de fin sélectionnée par l'utilisateur.

    parametre()
        Description : Gère les paramètres d'acquisition de données, y compris la sélection des coordonnées, l'option de saisie manuelle ou l'importation d'un fichier GEOJSON, la sélection de la plage de dates et le démarrage du script pour télécharger et traiter les données.
        Entrée : Aucune.
        Sortie : Aucune.

    run_script(bbox, start_date, end_date, save_file_name)
        Description : Exécute le script pour télécharger et traiter les données correspondant aux paramètres spécifiés.
        Entrée : bbox (list) - Les coordonnées de la bounding box au format [min_lon, min_lat, max_lon, max_lat], start_date (datetime) - Date de début sélectionnée par l'utilisateur, end_date (datetime) - Date de fin sélectionnée par l'utilisateur, save_file_name (str) - Nom du fichier de sauvegarde.
        Sortie : Aucune.

    data_ndvi_dl(bbox, start_date, end_date, save_file_name)
        Description : Télécharge les données NDVI à partir d'un catalogue STAC en fonction des paramètres spécifiés.
        Entrée : bbox (list) - Les coordonnées de la bounding box au format [min_lon, min_lat, max_lon, max_lat], start_date (datetime) - Date de début sélectionnée par l'utilisateur, end_date (datetime) - Date de fin sélectionnée par l'utilisateur, save_file_name (str) - Nom du fichier de sauvegarde.
        Sortie : data (obj) - Données NDVI téléchargées.

    data_other_dl(bbox, start_date, end_date, save_file_name)
        Description : Télécharge d'autres données à partir d'un catalogue STAC en fonction des paramètres spécifiés.
        Entrée : bbox (list) - Les coordonnées de la bounding box au format [min_lon, min_lat, max_lon, max_lat], start_date (datetime) - Date de début sélectionnée par l'utilisateur, end_date (datetime) - Date de fin sélectionnée par l'utilisateur, save_file_name (str) - Nom du fichier de sauvegarde.
        Sortie : data (obj) - Autres données téléchargées.

Lissage_sav_gol_weighted.py
Fonctionnalités

    Sélection manuelle des coordonnées géographiques ou import d'un fichier GEOJSON pour définir la région d'intérêt.
    Sélection de l'intervalle de temps pour la récupération des données.
    Affichage d'une carte Folium pour visualiser la région d'intérêt et la bbox.
    Téléchargement des données à partir d'un catalogue STAC à l'aide de l'API Planetary Computer.
    Traitement des données téléchargées.
    Affichage des premières tranches temporelles des données téléchargées avec Matplotlib.

Fonctions

    lister_fichiers_dossier(dossier_path)
        Description : Liste les fichiers dans un dossier filtrés par ceux commençant par '1_'.
        Entrée : dossier_path (str) - Chemin du dossier à lister.
        Sortie : fichiers (list) - Liste des noms de fichiers filtrés.

    preprocess_lissage(data)
        Description : Prétraitement des données pour le lissage.
        Entrée : data (xarray.Dataset) - Dataset à prétraiter.
        Sortie : data (xarray.Dataset) - Dataset prétraité.

    lissage(ds, window, data_to_smooth, data_smooth, compteur=0)
        Description : Fonction de lissage des données.
        Entrée : ds (xarray.Dataset) - Dataset à lisser, window (int) - Taille de la fenêtre de lissage, data_to_smooth (str) - Nom de la variable à lisser, data_smooth (str) - Nom de la variable lissée, compteur (int, facultatif) - Compteur de lissage.
        Sortie : ds (xarray.Dataset) - Dataset lissé.

    all_pix(data_subset, window_selected)
        Description : Applique la fonction de lissage à tous les pixels.
        Entrée : data_subset (xarray.Dataset) - Subset de données à traiter, window_selected (int) - Taille de la fenêtre de lissage sélectionnée.
        Sortie : data_subset (xarray.Dataset) - Subset de données traité.

    split_data(data)
        Description : Divise les données en quatre parties.
        Entrée : data (xarray.Dataset) - Dataset à diviser.
        Sortie : part_1, part_2, part_3, part_4 (tuple) - Quatre parties de données divisées.

    save_to_netcdf(ds_results, chemin_fichier)
        Description : Sauvegarde le dataset dans un fichier NetCDF.
        Entrée : ds_results (xarray.Dataset) - Dataset à sauvegarder, chemin_fichier (str) - Chemin du fichier de sortie.
        Sortie : None

    display_first_image(ds, origin)
        Description : Affiche la première image du dataset.
        Entrée : ds (xarray.Dataset) - Dataset contenant les données, origin (str) - Origine de l'image ('upper' ou 'lower').
        Sortie : None

    main()
        Description : Fonction principale du script.
        Entrée : None
        Sortie : None

Visage.py
Fonctionnalités

    Exécution du Moniteur BFAST.
    Analyse des Pixels.
    Génération de Cartes.

Fonctions

    run_bfastMonitor(raster, dates, h, freq, monitorFreq, historic_period, dateCorr)
        Description : Cette fonction exécute la surveillance temporelle des séries chronologiques raster en utilisant l'algorithme BFAST (Breaks For Additive Seasonal and Trend) pour détecter les changements temporels significatifs.
        Entrée : 
                raster - Un tableau multidimensionnel représentant les données raster, 
                dates - Une liste de dates correspondant aux données raster, 
                h - La fraction d'observations utilisées pour estimer la composante saisonnière, 
                freq - La fréquence de surveillance (par exemple, 'jour', 'mois', 'année'),
                monitorFreq - La période de temps à surveiller, 
                historic_period - La période historique à considérer pour la surveillance, 
                dateCorr - Un paramètre pour la correction de la date.
        Sortie : allChanges - Tableau des changements détectés, allMeans - Tableau des moyennes calculées, allMagnitudes - Tableau des magnitudes calculées.

    date_before_and_after(date_np, years)
        Description : Cette fonction calcule la date avant et après une date donnée en fonction du nombre d'années spécifié.
        Entrée : date_np - Date au format numpy, years - Nombre d'années à ajouter ou à soustraire.
        Sortie : date_before_np - Date avant, date_after_np - Date après.

    lister_fichiers_dossier()
        Description : Cette fonction liste les fichiers dans un dossier spécifié.
        Entrée : Aucune.
        Sortie : fichiers - Liste des noms de fichiers dans le dossier filtrés.

    calculate_l2_distance(time, ndvi_smooth, int_break_list)
        Description : Cette fonction calcule la distance L2 entre les données NDVI lissées et les points de rupture détectés.
        Entrée : time - Tableau des dates, ndvi_smooth - Tableau de données NDVI lissées, int_break_list - Liste des indices de points de rupture.
        Sortie : liste_l2_dist - Liste des distances L2 calculées, breakpointmax - Indice du point de rupture maximal.

    calculate_magni_pond(time, ndvi_smooth, index_to_highlight)
        Description : Cette fonction calcule la magnitude pondérée des changements détectés.
        Entrée : time - Tableau des dates, ndvi_smooth - Tableau de données NDVI lissées, index_to_highlight - Indice du point de rupture sélectionné.
        Sortie : abs_result - Résultat du calcul de la magnitude pondérée.

    calculate_disparity(time, ndvi_smooth, index_to_highlight)
        Description : Cette fonction calcule la disparité des changements détectés.
        Entrée : time - Tableau des dates, ndvi_smooth - Tableau de données NDVI lissées, index_to_highlight - Indice du point de rupture sélectionné.
        Sortie : procruste - Valeur de la disparité calculée.

    calculate_ndviratio(time, ndvi_smooth, index_to_highlight)
        Description : Cette fonction calcule le ratio NDVI pour les changements détectés.
     Entrée : time: Tableau des dates, ndvi_smooth: Tableau de données NDVI lissées, index_to_highlight: Indice du point de rupture sélectionné.

    Sortie : meanNDVI_absLogRatio - Résultat du calcul du ratio NDVI.

    process_pixel_changes(filtered_data, changes, x, y)
        Description : Cette fonction traite les changements détectés pour un pixel spécifique.
        Entrée : filtered_data - Données raster filtrées, changes - Tableau des changements détectés, x - Coordonnée x du pixel, y - Coordonnée y du pixel.
        Sortie : int_break_list - Liste des indices des points de rupture pour le pixel sélectionné, time - Tableau des dates, ndvi_smooth - Tableau de données NDVI lissées.

    plot_with_streamlit(dataset, ndvi_smooth, int_break_list, liste_l2_dist)
        Description : Cette fonction utilise Streamlit pour afficher un graphique interactif des données raster et des changements détectés.
        Entrée : dataset - Dataset xarray, ndvi_smooth - Tableau de données NDVI lissées, int_break_list - Liste des indices des points de rupture, liste_l2_dist - Liste des distances L2 calculées.
        Sortie : Affiche un graphique avec les données fournies.

    image_mean(ds)
        Description : Cette fonction calcule la moyenne des données raster.
        Entrée : ds - Dataset xarray.
        Sortie : Tableau des moyennes calculées.

Telechargement.py
Fonctionnalités

    Affichage de l'interface utilisateur.
    Sélection des fichiers à télécharger.
    Téléchargement des fichiers sélectionnés.

Fonctions

    lister_fichiers_dossier_1(dossier_path)
        Description : Cette fonction explore un dossier spécifié et retourne une liste des noms de fichiers qui commencent par "1_".
        Entrée : dossier_path - Chemin du dossier à explorer.
        Sortie : Liste des noms de fichiers commençant par "1_" dans le dossier spécifié.

    lister_fichiers_dossier_2(dossier_path)
        Description : Cette fonction explore un dossier spécifié et retourne une liste des noms de fichiers qui commencent par "2_".
        Entrée : dossier_path - Chemin du dossier à explorer.
        Sortie : Liste des noms de fichiers commençant par "2_" dans le dossier spécifié.

    lister_fichiers_RGB(dossier_path)
        Description : Cette fonction explore un dossier spécifié et retourne une liste des noms de fichiers se terminant par "rgb.tif".
        Entrée : dossier_path - Chemin du dossier à explorer.
        Sortie : Liste des noms de fichiers se terminant par "rgb.tif" dans le dossier spécifié.

    lister_fichiers_date(dossier_path)
        Description : Cette fonction explore un dossier spécifié et retourne une liste des noms de fichiers se terminant par "date.tif".
        Entrée : dossier_path - Chemin du dossier à explorer.
        Sortie : Liste des noms de fichiers se terminant par "date.tif" dans le dossier spécifié.

    download_file(file_path, file_name)
        Description : Cette fonction télécharge un fichier spécifié en utilisant un bouton de téléchargement.
        Entrée : file_path - Chemin complet du fichier à télécharger, file_name - Nom du fichier.
        Sortie : Téléchargement du fichier sélectionné.

A noter

    Faire attention aux chemins en cas de bug.
    Certains noms de fichier sont récupérés avec des index ou des noms de fichier.
    Faire tourner chaque étape pour éviter les bugs :
        Récupérer les données.
        Lisser les données.
        Appliquer BFASTmonitor.
        Générer les cartes.
        Télécharger les cartes.
    Un nombre insuffisant d’années peut poser un bug (6 ans et plus minimum car les cartes sont générées sur les 3 ans avant et après la date d’un point de rupture).
