from pathlib import Path
import streamlit as st
try:
    from pages.utils.visu import VisuSimple
except ModuleNotFoundError:
    from utils.visu import VisuSimple


def plot_chart(fichier_selectionne):
    
    path_ndvi = Path(fichier_selectionne)
    if not Path(path_ndvi).exists():
        st.error('Smoothing NDVI is not found', icon="🚨")

    visu = VisuSimple(path_ndvi = path_ndvi)
    
    st.text(f"Click on one pixel to visualize results")
    visu.folium_map()
    

def main():
    st.title("Visualisation")
    st.write("Clicking on a pixel of your choice will also display its raw / smoothed NDVI time series")
    data_path = "pages/data"

    # List data files in the data folder 
    fichiers_dans_dossier = list(Path(data_path).rglob("2_*nc"))
    
    # select with streamlit
    fichier_selectionne = st.selectbox("Select your smoothed NDVI time series:", fichiers_dans_dossier,key=1)
    
    plot_chart(fichier_selectionne=fichier_selectionne)
        

if __name__ == "__main__":
    main()
