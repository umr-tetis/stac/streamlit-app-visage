import xarray as xr
import streamlit as st
from rasterio.crs import CRS
from pages.utils.util import process_dataset, lister_fichiers_dossier
from pages.utils.Bfast import Bfast
from pages.utils.metrics import Mapping

def main():

    st.title("LSLAs change detection")
    st.write("In this section, BFASTm-L2 algorithm is applied to the smoothed NDVI time series. Multiple breakpoints are detected for each pixel, and the most sensitive to seasonal changes is selected. This is done by selecting the breakpoint having the highest magnitude of change (euclidean distance, refered to as L2, between the two 3-years time series - previously averaged annually - located at each part of each detected breakpoint median, showed to be sensitive to amplitude and number of seasons) (Ngadi et al., 2023). In addition to the date of change and magnitude of change, two other metrics aiming at highlighting seasonal changes are extracted and mapped: the before/after NDVI ratio (giving the direction of change), and the dissimilarity (Procrustes distance sensitive to number and length of season). Combined in a unique RGB composite map, those two metrics along with the magnitude of change allow for inferring main drivers of land use and land cover change in the area of interest (see Ngadi et al., 2024).")
    st.write("To run the change detection algorithm on your NDVI time series, please select your dataset, then validate the selection and press 'Run change detection' button")
    st.write("Once the change detection is done, press the 'Generate the maps' button to generate five maps : date of change, magnitude of change, before/after NDVI ratio, dissimilarity, RGB map combining the three last metrics to highlight changes and their main drivers thanks to a correspondance table")
    dossier_path = "pages/data"
    
    # Liste des fichiers dans le dossier filtrés 
    fichiers_dans_dossier = lister_fichiers_dossier(dossier_path)
    
    # Sélection du fichier
    fichier_selectionne = st.selectbox("Select your smoothed NDVI time series:", fichiers_dans_dossier,key=1)
    
    if 'dataset' not in st.session_state:
        st.session_state.dataset = None
    
    if st.button(f"{fichier_selectionne} is selected, ok ? "):
        #ouvrir dataset
        dataset = xr.open_dataset(f'pages/data/{fichier_selectionne}')
        
        dimx = dataset.dims["x"]
        dimy= dataset.dims["y"]
        dimtime = dataset.dims["time"]
        st.success(f"Open dataset : {fichier_selectionne} of size : {dimx}x{dimy} and time {dimtime}")
        
        st.session_state.dataset = dataset
        st.session_state.fichier_selectionne = fichier_selectionne

    if 'changes' not in st.session_state:
        st.session_state.changes = []

    if st.button("Run change detection"):
        dataset = st.session_state.dataset
        
        list_dates, ndvi_values, filtered_data = process_dataset(dataset = dataset)
        st.session_state.filtered_data = filtered_data
        
        bfast = Bfast(hfrac=0.25,freq=23,monitorFreq=3,historic_period=3,dateCorr=1)
        st.success("Bfastm_L2 initialized")
        st.session_state.changes, st.session_state.means, st.session_state.magnitudes = bfast.run_bfastMonitor(raster = ndvi_values, list_dates = list_dates)
        
        st.write("magnitude shape: " , st.session_state.magnitudes.shape)
        st.success(f" Breakpoint, means and magnitudes are computed")
        
    st.write("Map: ")   
    if st.button("Generate the maps"):
        
        fichier_selectionne = st.session_state.fichier_selectionne 
        changes = st.session_state.changes 
        dataset = st.session_state.dataset
        filtered_data = st.session_state.filtered_data
        
        Mapping.map(dataset= dataset, filtered_data=filtered_data, name=fichier_selectionne[:-3], changes=changes)
    
    
if __name__ == "__main__":
    main()
    
  




