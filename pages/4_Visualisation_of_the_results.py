from pathlib import Path
import streamlit as st
try:
    from pages.utils.visu import Visu
except ModuleNotFoundError:
    from utils.visu import Visu
    

def plot_chart(fichier_selectionne, data_path):
    
    string = Path(fichier_selectionne).stem.split("_stack")[0]
    path_ndvi = Path(data_path) / f"2_{string}.nc"
    if not Path(path_ndvi).exists():
        st.error('Smoothing NDVI is not process, please run it again', icon="🚨")
    path_list_csv = Path(data_path)  / f"{string}_list.csv"

    visu = Visu(path_stack=fichier_selectionne, path_ndvi= path_ndvi, path_list_csv=path_list_csv)
    
    st.text(f"Click on one pixel to visualize results")
    visu.folium_map()
    

def main():
    st.title("Visualisation of the results")
    st.write("In this section, the different maps (date of change, magnitude of change, before/after NDVI ratio, dissimilarity, RGB map combining the three last metrics to highlight changes and their main drivers thanks to a correspondance table) can be displayed.")
    st.write("Clicking on a pixel of your choice will also display its raw / smoothed NDVI time series and detected / selected breakpoints, along with their corresponding magnitudes. The selected breakpoint, having the greatest magnitude, is higlighted in bold. Such vizualization allows the user to chose to go back to the previous steps (NDVI time series smoothing) and eventually fine-tune the smoothing settings if necessary")
    data_path = "pages/data"
    data_path = "outputs"

    # List data files in the data folder 
    fichiers_dans_dossier = Path(data_path).rglob("*_stack.tif")
    
    st.image("Correspodance_table.jpg")
    # select with streamlit
    fichier_selectionne = st.selectbox("Select your smoothed NDVI time series:", fichiers_dans_dossier,key=1)
    
    plot_chart(fichier_selectionne=fichier_selectionne,data_path=data_path)
        

if __name__ == "__main__":
    main()

