import streamlit as st

# Image de fond avec alpha faible
st.markdown(
    """
    <style>
    .main {
        background-image: url("image_clic.jpg.png");
        background-size: cover;
        background-repeat: no-repeat;
        opacity: 0.8; /* Ajustez l'opacité ici */
    }
    </style>
    """,
    unsafe_allow_html=True
)


# Titre de la page
st.title("Seasonal change detection tool: highlighting LSLAs on your territory")

# Logo de l'application
st.image("LSLA_Senegal_JBourgoin.jpg", )
st.write("_Source: Jérémy Bourgoin, ©CIRAD_")

# Description de l'application
st.header("Preamble")
st.write("This tool is designed to detect, highlight and map seasonal changes in a given area and period of interest, traducing specific land use transitions such as the implementation of Large Scale Land Acquisitions (LSLAs). It is based on the processing of satellite image time series of normalized difference vegetation index (NDVI) acquired using MODIS sensor, and available for free since 2000 at a spatial resolution of 250m and a frequency of 16 days (product name: MOD13Q1). The times series are first downloaded, smoothed using weighted Savitsky-Golay algorithm and then processed using BFASTm-L2 algorithm to identify and select breakpoints sensitive to changes in time series seasonality (ie. in the amplitude, length of season or number of seasons) (see Ngadi et al., 2023). In addition to the change date, metrics aiming at highlighting seasonal changes are extracted and mapped: the magnitude of change (sensitive to amplitude and number of seasons), the before/after NDVI ratio (giving the direction of change), and the dissimilarity (Procrustes distance sensitive to number and length of season). Combined in a unique RGB composite map, those three metrics allow for inferring main drivers of land use and land cover change in the area of interest (see Ngadi et al., 2024). Once generated, five maps are available : date of change, magnitude of change, before/after NDVI ratio, dissimilarity, RGB map combining the three last metrics to highlight changes and their main drivers thanks to a correspondance table. The maps can be vizualized, and the time series (Raw, Smoothed) of each pixel can be displayed, along with the breakpoints detetected and their corresponding magnitudes (euclidean distance, referred to as L2). All the products can also be downloaded.")
st.header("Features")
st.markdown("""**The tool is composed of 5 features:**
-	**Defining the region and period of interest:** Selection of the study area and definition of the period of interest (minimum X years) for which the MODIS NDVI time series (MOD13Q1 product) will be downloaded
-	**NDVI time series smoothing:** application of the weighted Savitsky Golay filtering and choice of user tuning parameters, visualization of the smoothing result
-	**Change detection:** application of BFASTm-L2 algorithm (breakpoint detection and selection), extraction and mapping of the  metrics (date of change, magnitude of change, before/after NDVI ratio, dissimilarity, RGB map combining the three last  metrics)
-	**Products vizualization:** visualization of the time series and detected breakpoint for a selected pixel, vizualization of the generated maps (date of change, magnitude of change, before/after NDVI ratio, dissimilarity, RGB map combining the three last  metrics to highlight changes and their main drivers thanks to a correspondance table)
-	**Products download:** Raw NDVI time series, Smoothed NDVI time series, Date of change map, Magnitude of change map, NDVI ratio map, Dissimilarity map, RGB composite map (Red channel: Magnitude, Green: NDVI ratio, Blue: Dissimilarity)
""")
st.write("**References**")
st.write("NGADI SCARPETTA, Yasmine, LEBOURGEOIS, Valentine, LAQUES, Anne-Elisabeth, DIEYE, Mohamadou, BOURGOIN, Jérémy, BEGUE, Agnès (2023). BFASTm-L2, an unsupervised LULCC detection based on seasonal change detection–An application to large-scale land acquisitions in Senegal. International Journal of Applied Earth Observation and Geoinformation, vol. 121, p. 103379.")
st.write("NGADI SCARPETTA, Yasmine, LEBOURGEOIS, Valentine, DIEYE, Mohamadou, LAQUES, Anne-Elisabeth, BEGUE, Agnès (2024). Insight into large-scale LULC changes and their drivers through breakpoint characterization – An application to Senegal. International Journal of Applied Earth Observation and Geoinformation, In revision.")
st.write("NGADI SCARPETTA, Yasmine. From Land Cover to Land Use Systems Mapping:Detection and Characterization of Large Scale Agricultural Investments [LSAIs] from Satellite Imagery. Application to Senegal. PhD thesis, GAIA Doctoral School. Defended on 9 April 2024, 198 p.")
st.header("Getting started")
st.write("Please click on the **Defining the region and period of interest** page to begin!")

# Section FAQ
st.header("FAQ")
st.write("To do")

# Contactez-nous
st.header("Contact")
st.write("valentine.lebourgeois@cirad.fr / yasmine.ngadi_scarpetta@cirad.fr / simon.madec@cirad.fr")

# Pied de page
st.write("©CIRAD")