
import xarray as xr
import numpy as np
import rasterio as rio 
import numpy as np 
import matplotlib.pyplot as plt

xx = xr.open_dataset("/home/simon/Project/Visage/streamlit-app-visage/outputs/2_SEN_CSS_2000_2024.nc")

path_stack = "/home/simon/Project/Visage/streamlit-app-visage/outputs/SEN_CSS_2000_2024_stack.tif"

def min_max_norm(raster):
    return (raster - np.nanmin(raster)) / (np.nanmax(raster) - np.nanmin(raster))

with rio.open(path_stack) as src:
    # Get the bounds of the raster (left, bottom, right, top)
    bounds = src.bounds
    x1,y1,x2,y2 = bounds
    
    bbox = [(bounds.bottom, bounds.left), (bounds.top, bounds.right)] 
    raster_crs = src.crs
    
    # Read the data as an image (assuming 3 bands, e.g., RGB)
    dissimilarity = src.read(3)
    #dissimilarity = min_max_norm(src.read(3))
    dissimilarity = dissimilarity*100/255
    
    ndviratio = src.read(2)
    ndviratio[ndviratio==-3000] = np.nan 
    #ndviratio = min_max_norm(ndviratio)
    ndviratio = ndviratio*100/255
    
    
    magnitude = min_max_norm(src.read(1))
    
    magnitude_pondere = src.read(4)
    magnitude_pondere = min_max_norm(magnitude_pondere)
    
    # Plot histogram
    date = src.read(5)
    
    rgb = np.stack((magnitude,dissimilarity,ndviratio), axis=-1)
    
    
    
